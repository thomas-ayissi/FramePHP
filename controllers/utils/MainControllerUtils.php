<?php
    /*
     * Przykłądowa klasa dodatków do kontrolerów. Plik ite, jeżeli mają być autmatycznie dołączane, muszą mieć
     * nazwę "[kontroler]Utils.php".
     */

    class MainUtils
    {
        function getUserIP()
        {
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = $_SERVER['REMOTE_ADDR'];

            if(filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
            elseif(filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
            else $ip = $remote;

            return $ip;
        }
    }