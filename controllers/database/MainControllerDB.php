<?php
    /*
     * Przykłądowy plik bazy danych, który jest automatycznie dołączany do kontrolera o odpowiedniej nazwie,
     * jeżeli ten plik nazywa się "[kontroler]DB.php" i za pomocą tworzenia
     * zwykłego obiektu tam wykorzytywany. Dziedziczy on po stworzonej wcześniej klasie abstrakcyjnej, która umożliwa
     * połączenie z hostem i bazą wskazaną w pliku konfiguracyjnym.
     */
    class ExampleDBClass extends Database
    {
        public function getExampleUser($user, $pass)
        {
            $this->query("SELECT * FROM `users` WHERE `login`='".$user."' AND `pass`='".sha1($pass)."'");

            if($this->numOfRes() == 1) echo "Użytkownik istnieje i może się zalogować!";
            else echo "Brak użtykownika o podanych danych!";
        }
    }