﻿Mały framework "PHPFrame" tworzony przez PSCom.

Tworzony na własne potrzeby i w miarę możliwości rozbudowywany o kolejne funkcjonalności,
które mogą okazać się przydatne bądź będą uniwersalne.

~~~~~~~~~~~~ Struktura:

//folder rodzic folderu web - ustawionego w konfiguracji apache strony
|
|-[controllers]
| |
| |-[database]
| | |
| | |-Database.php //klasa abstrakcyjna logiki biznesowe
| | |
| | |-//inne pliki logiki biznesowej
| |
| |-[utils]
| | |
| | |-//pliki dodatków do kontrolerów, możliwość podzielenia dużej ilości kodu
| |
| |-ErrorsController.php //domyślny kontroler błędów
| |-MainController.php //domyślny główny kontroller
|
|-[core]
| |
| |-[baseClasses] // folder zawiera bazowe klasy kontrolerów itp.
| | |-Controller.php //klasa abstrakcyjna potrzebna przy budowie własnych kontrolerów
| |
| |-Config.php //klasa konfiguracji
| |-Core.php //klasa jądra framworka, któa odpwoiada za routing automatyczny i utworzenei ścieżek do plików
|
|-[public_www] // folder web - ustawiony w konfiguracji apache
| |
| |-[static]
| | |
| | |-//pliki statyczne: js, css, obrazki itp.
| |
| |-.htaccess //plik przpisujący adrtes strony
| |-mainController.php //główny kontroler, to tu zaczyna się magia ;)
|
|-[views]
| |
| |-[ErrorsViews] //folder z widokami kontrolera błędów
| |-[MainViews] //folder z widokami głównego kontrolera
| |-//inne foldery widoków kontrolerów z plikami widoków funkcji
|
|-config.php // plik konfiguracyjny
|-README.txt // ten plik

TODO: proces dołaczania "dodatków" do frameworka
TODO: Dodaj dodatkowe funkcje frameworka, np. getUserIP jako autmatycznie includowany plik