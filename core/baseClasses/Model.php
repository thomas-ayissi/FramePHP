<?php

abstract class Model
{
    private $fieldNameTag = "%fieldName%";

    private $primaryField = "";
    private $uniqueFields = array();

    private $charSet = "utf8";
    private $collate = "utf8_unicode_ci";

    // $zmienna = funkcjaPola(args)
    // skrypt zew. pobiera wartość $zmiennej - polecenie SQL i
    // dodaje w odpowiednim miejscu nazwe zmiennej jako nazwe pola
    
    public function getCharSet()
    {
        return $this->charSet;
    }

    public function setCharSet($charSet)
    {
        $this->charSet = $charSet;
    }

    public function getCollate()
    {
        return $this->collate;
    }

    public function setCollate($collate)
    {
        $this->collate = $collate;
    }

    public function getPrimaryField()
    {
        return $this->primaryField;
    }

    public function getUniqueFields()
    {
        return $this->uniqueFields;
    }

    protected function setPrimaryField($fieldName)
    {
        $this->primaryField = $fieldName;
    }

    protected function addUniqueField($fieldName)
    {
        array_push($this->uniqueFields, $fieldName);
    }

    protected function charField($maxLength = 255, $default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` CHAR(".$maxLength.") ".$default.$comment;
        return $query;
    }

    protected function varcharField($maxLength = 255, $default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` VARCHAR(".$maxLength.") ".$default.$comment;
        return $query;
    }

    protected function textField($default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` TEXT ".$default.$comment;
        return $query;
    }

    protected function intField($maxLength = 5, $autoIncrement = false, $default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";
        $ai = $autoIncrement == false ? "" : " AUTO_INCREMENT";

        $query = "`".$this->fieldNameTag."` INT(".$maxLength.") ".$default.$ai.$comment;
        return $query;
    }

    protected function bigintField($maxLength = 20, $autoIncrement = false, $default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";
        $ai = $autoIncrement == false ? "" : " AUTO_INCREMENT";

        $query = "`".$this->fieldNameTag."` BIGINT(".$maxLength.") ".$default.$ai.$comment;
        return $query;
    }

    protected function dateField($default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` DATE ".$default.$comment;
        return $query;
    }

    protected function datetimeField($default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` DATETIME ".$default.$comment;
        return $query;
    }

    protected function timeField($default = "", $comment = "")
    {
        $default = $default == "" ? "NULL DEFAULT NULL" : "NOT NULL DEFAULT '".$default."'";
        $comment = $comment == "" ? "" : " COMMENT '".$comment."'";

        $query = "`".$this->fieldNameTag."` TIME ".$default.$comment;
        return $query;
    }
}