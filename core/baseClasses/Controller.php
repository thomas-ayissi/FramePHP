<?php
/*
 * Abstrakcyjna klasa kontrolerów, która zawiera podstawowe parametry i funkcje potrzebne do funkcjonowania
 * tworzonych przez programistę kontrolerów. Raczej nie obędzie się bez dziedziczenia od niej.
 */

abstract class Controller
{
    private $function;              // funkcja kontrolera potrzebna do wybrania widoku
    private $controller;            // nazwa kontrolea
    private $args = array();        // tablica argumentów wewnątrz klasy
    private $whichView;             // odpowiedni widok
    private $viewVars = array();     // zmienna zmiennych widoku - wartości przekazywanych do widoku

    // Konstruktor przypisuje odpowiednie wartości do wewnętrznych zmiennych i tworzy ścieżkę do widoku
    function __construct($controllerName, $funcName, $args)
    {
        $this->controller = $controllerName;
        $this->function = $funcName;
        $this->args = $args;
        $this->whichView = "../views/".$controllerName."Views/".$funcName."View";
    }

    // Domyślan funkcja index, któa nic nie robi
    public function index()
    {}

    // Na koniec żywota obiektu kontrolera załaczany jest odpowiedni widok, pomocnicza klasa statyczna widoku i
    // wypakowana tablica zmiennych widoku
    function __destruct()
    {
        if(@method_exists($this, $this->function))
        {
            require_once("../core/baseClasses/View.php");

            extract($this->viewVars);
            if (@!include($this->whichView . ".php")) Config::msg("Brak widoku.", "badPage");
        }
    }

    // Pobiera argument z $url o odpowiednim numerze w tablicy(od 0)
    protected function arg($num)
    {
        if(!isset($this->args[$num])) return "";
        else return $this->args[$num];
    }

    // Pobiera nazwę funkcji
    protected function getFunction()
    {
        return $this->function;
    }

    // Pobiera nazwę kontrolera
    protected function getController()
    {
        return $this->controller;
    }

    // Pozwala dodać zmienną do zmiennnych widoku
    protected function addViewVar($var, $val)
    {
        $this->viewVars[$var] = $val;
    }

    // Pozwala ustawić zmienną widoku
    protected function setViewVars($array)
    {
        $this->viewVars = $array;
    }

    //Przekierowuje na podany adres
    protected function redirect($url)
    {
        header("Location: ".$url);
        exit();
    }
}