<?php
    /*
     * Abstrakycyjne klasa logiki biznesowej, któa umożliwi pobieranie danych z bazy danych i
     * zawarcie tego wszystkiego w osobnych klasach, które będą potem tylko załaczane do kontroleów.
     */

    abstract class Database
    {
        private $db;
        private $query;

        function __construct()
        {
            $this->connect();
        }

        function __destruct()
        {
            $this->close();
        }

        // Funkcja zwraca wyniki w postaci tablicy asocjacyjnej
        protected function query($queryText)
        {
            $this->query = $this->db->query($queryText);
        }

        protected function fetchAss()
        {
            return $this->query->fetch_assoc();
        }

        protected function numOfRes()
        {
            return $this->query->num_rows;
        }

        private function connect()
        {
            $this->db = new mysqli(Config::get("host"), Config::get("user"),
                Config::get("pass"), Config::get("database"));

            if($this->db->connect_errno)
                Config::msg("Błąd połączenia z bazą danych! :: ".$this->db->connect_error, "badPage");
        }

        private function close()
        {
            @$this->db->close();
        }
    }