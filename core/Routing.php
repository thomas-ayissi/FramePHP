<?php

    /**
     * Klasa routingu. Przechwytuje tablicę routingu, organizuje ją itd.
     */

    class Routing
    {
        // tablica routingu
        static private $routing = [
            "/main/"            => "Main",
            "/errors/"          => "Errors",
        ];

        // ustawia/zmienia wartości funkcji
        static function set($url, $controllerName)
        {
            self::$routing[$url] = $controllerName;
        }

        // pobiera kontroler według podanego adresu
        static function getController($url)
        {
            return self::$routing[$url];
        }

        // pobiera adres według podanego kontrolera
        static function getUrl($controller)
        {
            $controller = ucfirst($controller);

            foreach (self::$routing as $key => $val)
            {
                if($val == $controller)
                    return $key;
            }

            return -1;
        }
    }