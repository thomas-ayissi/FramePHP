<?php
    /*
     * Klasa konfiguracyjna. To tutaj znajdują się wszystkie funkcje odpowiedzialne
     * za koordynację i kontrolę pracy samego frameworka, a nie aplikacji.
     */

    require_once("Routing.php");

    class Config
    {
        // Zmienna config posiada domyślne wartości flag.
        static private $config = [
            "mode"                  => "dev",
            "defaultController"     => "Main",
            "errorsController"      => "Errors",
            "dbHost"                => "localhost",
            "dbDatabase"            => "",
            "dbUser"                => "",
            "dbPass"                => "",
            "adminEmail"            => "",
            "contactEmail"          => "",
        ];

        // Funkcja pozwalajaca zmieniać wartości flag w $config
        static function set($flag, $val)
        {
            self::$config[$flag] = $val;
        }

        // Funkcja pobiera wartości flag z $config
        static function get($flag)
        {
            return self::$config[$flag];
        }

        // Funkcja wykonuje odpowiednią akcję informacyjną w zależności od wartości flagi "mode"
        static function msg($msg, $pageError)
        {
            if(self::checkFlag("mode"))
            {
                if (self::$config['mode'] == "dev") echo $msg;
                elseif (self::$config['mode'] == "prod")
                {
                    header("Location: ".Routing::getUrl(Config::get("errorsController"))."" . $pageError);
                    exit();
                }
            } else echo "Błąd konfiguracji!";

        }

        // Sprawdza istnienie flagi w config i zwraca odpowiedni stan
        static private function checkFlag($flag)
        {
            if(isset(self::$config[$flag])) return true;
            else return false;
        }
    }