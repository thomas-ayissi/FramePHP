<?php
    /*
     * Klasa odpowiada za wywoływanie odpowiedniego kontrolera w zależności od wartości zmiennej $url.
     * Wskauje on także na podstawie w.w. zmiennej, jaka funckja w klasie kontrolera ma zostać wywołana
     * oraz przekazuje kolejne argumenty do funkcji w formie tablicy $args.
     */

    require_once("Routing.php");
    require_once("Config.php");

    class Core
    {
        // Główna funkcja wykonująca pracę dispachera
        public function dispatcher($url)
        {
            // Zmienne aby
            $controller = 0; $function = 0; $args = array();
            // Zamienia tablicę klucz=>wartość na zmienne
            extract($this->route($url));

            $controllerFile = $controller."Controller";

            // Załącza plik bazowej klasy kontrolera
            require_once("baseClasses/Controller.php");
            // Załącza plik danego kontrolera
            if(@include("../controllers/".$controllerFile.".php"))
            {
                // Załącza klasę abstrakcyjną Database potrzebnądo działania klas baz danych kontrolerów i odpowiedni
                // plik bazy danych.
                include("baseClasses/Database.php");
                @include("../controllers/database/".$controllerFile."DB.php");

                //Załącza klasę dodatków - utils do kontrolerów automatycznie
                @include("../controllers/utils/".$controllerFile."Utils.php");

                if(@class_exists($controller))
                    $controllerOb = new $controller($controller, $function, $args);
                else
                    Config::msg("Brak klasy kontrolera", "badPage");

                // Sprawdza istnienie metody w kontrolerze
                if(@method_exists($controllerOb, $function))
                    $controllerOb->$function();
                else Config::msg("Brak funkcji.", "badPage");
            } else Config::msg("Brak kontrolera.", "badPage");
        }

        // Metoda przerabia podany adres url(zmienną $url z mainController) na odpowiednie zmienne.
        private function route($url)
        {
            $varsTab = array();

            $url = explode("/", $url);
            /*
             * Jeżeli brak nazwy kontrolera(sama domena) to przechodzi do głównego kontrolera(main) i funkcji
             * W konfiguracji można wskazać nazwę głównego kontrolera, jednak zawsze będzie ona dostępna
             * pod adresem /main
             */
            $allUrl = "";
            $routeUrl = "";

            for($i = 0; $i < (count($url)-1); $i++)
                $routeUrl.=$url[$i]."/";

            for($i = 0; $i < count($url); $i++)
            {
                if($i == (count($url)-1))
                    $allUrl .= $url[$i];
                else
                    $allUrl .= $url[$i] . "/";
            }

            if(empty($url[1])) // jeżeli uzytkownik wszedl na strone lub nie podal zadnego url
            {
                if(Routing::getUrl(Config::get("defaultController")) == -1)
                    Config::msg("Brak routingu", "badPage");
                else
                    header("Location: ".Routing::getUrl(Config::get("defaultController"))."index");
                exit();
            }
            else $varsTab["controller"] = Routing::getController($routeUrl);

            //Jeżeli brakuje nazwy funkcji, to przekierowuje na .../index
            if($allUrl == $routeUrl)
            {
                header("Location: ".Routing::getUrl($varsTab["controller"])."index");
                exit();
            }
            else $varsTab["function"] = $url[count($url)-1];

            // Odcina argumenty przekazane w $url i przypisuje je do tablicy jako później dostępną zmienną $args
            $varsTab["args"] = array_slice($url, 3);

            return $varsTab;
        }
    }
