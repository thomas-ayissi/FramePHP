<?php
    /*
     * Główny kontroler, który odpowiada za pierwszą styczność klienta z aplikacją. To tutaj
     * ładowane sąodpowiednie kontrolery, z nich widoki i przekazywany jest model.
     * Ładowana jest tutaj także konfiguracja aplikacji oraz routing.
     *
     * Dzięki plikowi .htaccess to ten plik jest wykonywany jako pierwszy, a wszystko co zostanie wpisane
     * w adres przekazywane jest jako argument do zmiennej $action.
     *
     * W folderze static/ , który jest obecny w aktualnym folderze znajdują się wszystkie pliki statyczne:
     * JS, CSS, obrazki itp.
     */
    require_once("../config.php");
    require_once('../core/Core.php');

    $url = htmlspecialchars($_GET['action']);

    $core = new Core();
    $core->dispatcher($url);
