<?php

class test1 extends Model
{
    public $id;
    public $s1, $s2;
    public $userName;
    public $password;
    public $email;

    public function __construct()
    {
        $this->id           = $this->intField(11, true);
        $this->s1           = $this->intField(12);
        $this->s2           = $this->intField(13);
        $this->userName     = $this->charField(50);
        $this->password     = $this->charField(50);
        $this->email        = $this->charField();

        $this->setCharSet("utf8");
        $this->setCollate("utf8_unicode_ci");

        $this->addUniqueField("s1");
        $this->setPrimaryField("id");
    }
}

class test2 extends Model
{
    public $id;
    public $name;
    public $rang;

    public function __construct()
    {
        $this->id       = $this->intField(11, true);
        $this->name     = $this->charField(50);
        $this->rang     = $this->charField(200, "user");

        $this->setCharSet("utf8");
        $this->setCollate("utf8_unicode_ci");

        $this->setPrimaryField("id");
    }
}