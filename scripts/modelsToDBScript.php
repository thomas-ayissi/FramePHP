<?php
    /*
     * 1. przeszukuje katalog /models w poszukiwaniu plików modeli [przedrostekGrupyTabel]Model.php
     * 2. Przeszukuje plik w poszukiwaniu klas(for/while)
     * 3. includuje plik,
     *    2.2. Wywołuje wszystkie zmienne klasy i łączy je w jedno zapytanie SQL tworzenia taeli
     *    2.3. Dodaje odpowiednie pola primary i unique
     *    2.4. Tabela ma nazwę "[przedrostekGrupyTabel]_[nazwaDanejKlasy]"
     *    2.5. Wysyła zapytanie utworzenia tabeli
     *    2.6. Wyświetla komunikat o niepowodzeniu/powodzeniu/istnieniu danej klasy w bazie danych
     *
     */

// skanowanie folderu
$modelDirItems = scandir("../models/");
$modelFiles = array();

@require_once("../core/baseClasses/Model.php");
@require_once("../config.php");

foreach ($modelDirItems as $item)
{
    // wzór naazewniczy
    $regPattern = "/^.*(Model.php)$/";
    
    // jeżeli nie jest katalogiem i zgadza się ze schematem nazwenictwa [przedrostekGrupyTabel]Model.php
    if(!is_dir($item) && preg_match($regPattern,$item))
    {
        // wydziela nazwe grup tabel
        $tableGroupName = preg_split("/(Model.php)$/", $item);
        $tableGroupName = $tableGroupName[0];

        $file = @fopen("../models/".$item, "r");
        $line = "";
        flock($file, 1);

        // jezeli otwarcie sie powiodło
        if($file)
        {
            include_once("../models/".$item);

            // do końca pliku
            while ($line = fgets($file))
            {
                // jeżeli jest to klasa
                if(preg_match("/^(class)(\s)(.*)(\s)(extends)(\s)(Model)/", $line))
                {
                    // przypisanie nazwy klasy
                    $className = preg_split("/^(class)(\s)/", $line);
                    $className = preg_split("/(\s)(extends)(\s)(Model)/" ,$className[1]);
                    $className = $className[0];

                    // wyciąga publiczne pola klasyd
                    $classVars = get_class_vars($className);
                    $class = new $className();

                    $sqlQuery = "CREATE TABLE `".$tableGroupName."_".strtolower($className)."` (";

                    $i = 1;
                    foreach($classVars as $name => $val)
                    {
                        $sqlQuery .= str_replace("%fieldName%", $name, $class->$name);

                        if($i < count($classVars))
                            $sqlQuery .= ", ";

                        $i++;
                    }

                    // dodawanie pól uique
                    $i = 1;
                    foreach($class->getUniqueFields() as $val)
                    {
                        $sqlQuery .= ", UNIQUE (`".$class->getUniqueFields()[$i-1]."`)";

                        $i++;
                    }

                    // dodawanie pola primary jako ostatniego
                    $sqlQuery .= ", PRIMARY KEY (`".$class->getPrimaryField()."`)";

                    $sqlQuery .= ");";

                    //echo $sqlQuery."\n";

                    @$mysqli = mysqli_connect(Config::get("dbHost"), Config::get("dbUser"), Config::get("dbPass"), Config::get("dbDatabase"));

                    // sprawdzanie bledów
                    if(mysqli_connect_error())
                        echo mysqli_connect_error();
                    else
                    {
                        //tabela nie istnieje
                        if(!mysqli_query($mysqli, "SELECT 1 FROM ".$tableGroupName . "_" . strtolower($className)." LIMIT 1"))
                        {
                            // poprawne zapytanie
                            if (mysqli_query($mysqli, $sqlQuery))
                            {
                                mysqli_query($mysqli,
                                    "ALTER TABLE `auth_users` DEFAULT CHARACTER SET " . $class->getCharSet() . " COLLATE " . $class->getCollate());

                                echo "Adding table " . $tableGroupName . "_" . strtolower($className) . " success!\n";
                            }
                            else
                            {
                                echo "!!! --- Error with adding table " . $tableGroupName . "_" . strtolower($className) . "\n";
                                echo mysqli_error($mysqli);
                            }
                        }
                        else
                            echo "Table ".$tableGroupName . "_" . strtolower($className) . " exist!\n";
                    }
                }
            }
        }

        flock($file, 3);
        fclose($file);

        // załącza plik

    }
}
