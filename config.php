<?php
    require_once("core/Config.php");
    require_once("core/Routing.php");

    /*
     * ~~~~~~~~~~~~~~~~~~~~~~~~ Zmienne globalne ~~~~~~~~~~~~~~~~~~~~~~~~
     */

    define("STATIC_DIR",    "/static/");
    define("ADDS_DIR",      "/static/adds/");
    define("CSS_DIR",       "/static/css/");
    define("FONTS_DIR",     "/static/fonts/");
    define("IMAGES_DIR",    "/static/images/");
    define("JS_DIR",        "/static/js/");
    define("VIEW_DIR",      "../views/");
    define("FRAG_DIR",      "../views/fragments/");

    /*
     * ~~~~~~~~~~~~~~~~~~~~~~~~ Konfiguracja ~~~~~~~~~~~~~~~~~~~~~~~~
     * Zmienne przekazywane do konfiguacji. Odradzam usuwanie zmiennej całkowicie.
     */
    // Zmienia podejście aplikacji do użytkownika. Tryb rozwojowy lub realizacyjny.
    // Wartości: dev/prod
    Config::set("mode",                 "dev");
    // Domyślny kontroler, który zostaje wywołany gdy jest sama domena.
    Config::set("defaultController",    "Main");
    // Kontroler błędów, które są dostępne w [mode => prod]
    Config::set("errorsController",     "Errors");
    // Dane do logowania się MySQL
    Config::set("dbHost",                "localhost");
    Config::set("dbDatabase",            "");
    Config::set("dbUser",                "");
    Config::set("dbPass",                "");
    // Adres e-mail administratora i kontaktowy
    Config::set("adminEmail",           "");
    Config::set("contactEmail",         "");

    /*
     * ~~~~~~~~~~~~~~~~~~~~~ Routing ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * Tablica url -> controller
     */
    // Z tablicy przechwytywany jest adres i wskazuje na kontroller, który ma zostać użyty. Potem jeszcze wpisywana jest funkcja do użycia.
    Routing::set("/main/", "Main");
    Routing::set("/errors/", "Errors");

    /*
     * ~~~~~~~~~~~~~~~~~~~~~~~~ Aspekty globalne ~~~~~~~~~~~~~~~~~~~~~~~~
     * Zdarzenia, zmienne itp., któe muszą zostać wywołane globalnie w całej aplikacji.
     */

    session_start();
