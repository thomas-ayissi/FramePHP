<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />

        <title><?php echo $title; ?></title>

        <link rel="stylesheet" href="<?php echo STATIC_DIR; ?>adds/semantic-ui/semantic.min.css" />

        <style>
            div#TITLE {
                margin-top: 10px;
                text-align: center;
                font-size: 22px;
            }

            div#TEXT {
                font-size: 16px;
            }

            div#FOTTER {
                font-size: 12px;
                color: gray;
            }

            em {
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <div class="ui grid">
            <div class="four wide column"></div>
            <div class="eight wide column">
                <div id="TITLE" class="ui blue top attached inverted segment">
                    FramePHP
                </div>
                <div id="TEXT" class="ui segment attached">
                    <p><em>Witam!</em></p>
                    <p>Dzięki za używanie mojego FramePHP. Jest to jak na razie niewielki projekt,
                    który miał mi pomóc tworzyć moje projekty na modelu MVC. Potem ta myśl rozwinęła się na
                    coś większego do czego teraz prowadzi.</p>
                    <p><em>Życzę miłej pracy!</em></p>
                </div>
                <div id="FOTTER" class="ui segment bottom attached">
                    Autor: PSCom
                </div>
            </div>
            <div class="four wide column"></div>
        </div>
    </body>
</html>