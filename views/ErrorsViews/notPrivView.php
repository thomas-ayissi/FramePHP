<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>

        <title>Błąd 511</title>

        <link href="<?php echo STATIC_DIR; ?>adds/semantic-ui/semantic.min.css" rel="stylesheet" />

        <style>
            body {
                margin-top: 50px;
                font-size: 22px;
            }

            #TITLE {
                font-size: 32px;
                text-align: center
            }

            #TEXT {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="ui grid">
            <div class="four wide column"></div>
            <div class="eight wide column">
                <div id="TITLE" class="ui red inverted top attached warning segment">
                    <i class="warning icon"></i>
                    Błąd 511
                </div>
                <div id="TEXT" class="ui bottom attached segment">
                    Aby uzyskać dostęp do strony musisz być zalogowany!
                </div>
            </div>
            <div class="four wide column"></div>
        </div>
    </body>
</html>